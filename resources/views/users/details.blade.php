@extends('layouts.app')
  
@section('content')  
@section('title', 'info')
<div class="row">
<div class="col-sm-8 offset-sm-2">

<h2 class="text-center">User Page</h2>
<div>
<label for = "name"> user id:</label>
    {{$users->id}} 
</div>
<div>
<label for = "id"> user name: </label>
    {{$users->name}} 
    </div>
<div>
<label for = "email"> user email: </label>
{{ $users->email}} </div> 
<div>
<div>
@if(Auth::user()->id == 1)
<label for = "email"> user role: </label>
{{ $users->email}} </div> 
@endif

<label for = "department_id"> user department: </label>@if(Gate::allows('change-department'))
                <select class="form-control" name="department_id">                                                                         
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}"> 
                            {{ $department->name }} 
                        </option>
                    @endforeach    
                    </select>
            @else
                {{$users->department->name}}
            @endif
        </div>
 
        <div class="form-group">
            <input type = "submit" class="btn btn-outline-dark btn-lg" name = "submit" value = "change department">
        </div>

<div>



        </div>                    
    </form> 
        @endsection
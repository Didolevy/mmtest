@extends('layouts.app')

@section('title', 'User')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List Of Users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>role</th><th>Details</th><th>delete</th><th>changes</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>@if(isset($user->department_id))
                          {{$user->department->name}}  
                        @else
                          No Assigned owner
                        @endif
            </td>
       
                    <td>   <select class="form-control" name="role_id"> 
                        @foreach(App\Role::roleuser($user->id) as $role)
                        <option value="{{ $role->id}}"> 
                           {{$role->name}}
                        </option>
                        @endforeach  
                      </td>
            <td>
            <a class="navbar-brand" href="{{route('user.details',$user->id)}}">User Details</a>
            </td> 
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>
        
            <td>
            
            <button type="button" class="btn btn-secondary">Make Mangear</button>
            </td>

        </tr>
    @endforeach
</table>
@endsection


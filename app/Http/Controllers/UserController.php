<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Role;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $departments = Department::all();
        $roles=Role::all();

        return view('users.index', compact('users','departments','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users');
    }

    public function details($id)
    {        
        $users=User::findOrFail($id);
        $departments = Department::all();
        return view('users.details',compact('users','departments'));
        //$departments = Department::all();
        //$users = User::all();
        //return view('users.index', compact('users','departments'));       
        
    }
    public function makeadmin($id, $rid)
    {
        if(Gate::allows('make', $user))
        {
      
        $user = User::findOrFail($id);
        $role->role_id = 2 ;
        $user->save();
        Session::flash('the change happende'); 
        return back();

        }
    }
    
    public function changedepartment(Request $request){
        $uid = $request->id;
        $did = $request->department_id;
        $user = User::findOrFail($uid);
        if(Gate::allows('change-department', $user)){
            $from = $user->department->id;
            $user->department_id = $did;
            $user->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the department');
        }
        
        return redirect('users');

    }
}
